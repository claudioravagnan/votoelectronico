
<?php
session_start();
if(!isset($_SESSION['rol'])){
 //sesion no iniciada
  header("Location:index.html"); 

}
else{
    //sesion iniada correctamente
    $nivel = $_SESSION['rol'];
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/EEST N°1.png" class="img-fluid">
        </div>
    </div>
    </div>
    <div class="container-fluid">

        <div class="alert alert-dark" style="margin-top:  10px;;" role="alert">
            <center>
                <strong> SISTEMA DE VOTO ELECTRONICO ESTUDIANTIL </strong>
            </center>
        </div>

    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index2.html">Inicio</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle disebled" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Alumnos
                </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="alumnos1.php">Alta de alumnos</a></li>
                            <li><a class="dropdown-item" href="consultaalumnos.php">Consultas/Bajas/Modificaciones</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Turnos
                </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="turno.html">Alta de turno</a></li>
                            <li><a class="dropdown-item" href="consultaturno.php">Consultas/Bajas/Modificaciones</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>

                        </ul>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Listas
                </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="altalista1.php">Alta de Lista</a></li>
                            <li><a class="dropdown-item" href="consultalista.php">Consultas/Bajas/Modificaciones</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item">


                        <?php
                    if($nivel==2){

                       // DIRECTOR
                       echo '<a class="nav-link active" aria-current="page" href="resultados.php">Resultados</a>';
                    }else{
                        echo '<a class="nav-link disbled" aria-current="page" href="#">Resultados</a>';
                    }
                        ?>


                    </li>
                    <li class="nav-item">

                    <?php
                    if($nivel==1){

                       // AUTORIDAD DE MESA
                       echo '<a class="nav-link active" aria-current="page" href="huella.php">Habilitacion de mesa</a>';
                    }else{
                        echo '<a class="nav-link disbled" aria-current="page" href="#">Habilitacion de mesa</a>';
                    }

                        ?>
                        


                    </li>
                </ul>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/ELECCIONES.png" class="img-fluid">
        </div>
    </div>
    </div>


    <footer class="bg-light text-center text-lg-start">
        <!-- Copyright -->
        <div class="text-center p-3 container-fluid style=" background-color: rgba(0, 0, 0, 0.2); ">
        © 2021 Copyright: realizado por 7mo - informatica E.E.S.T. N°1 "Don Luis Bussalleu - Rojas (B) "
        
      </div>
      <!-- Copyright -->
    </footer>




    
</div>




    <script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
</body>

</html>