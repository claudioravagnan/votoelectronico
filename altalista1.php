<?php
$nivel = $_GET['rol'];
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>

    <body>
        <div class="container-fluid">
            <div class="full-whidth-image">
                <img src="img/EEST N°1.png" class="img-fluid">
            </div>
        </div>
        </div>
        <div class="container-fluid">

            <div class="alert alert-success" style="margin-top:  10px;;" role="alert">
                <center>
                    <strong> SISTEMA DE VOTO ELECTRONICO ESTUDIANTIL </strong>
                </center>
            </div>

        </div>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index2.php">Inicio</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Alumnos
                </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="alumnos1.php">Alta de alumnos</a></li>
                                <li><a class="dropdown-item" href="consultaalumnos.php">Consultas/Bajas/Modificaciones</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>

                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Turnos
                </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="turno.html">Alta de turno</a></li>
                                <li><a class="dropdown-item" href="consultaturno.php">Consultas/Bajas/Modificaciones</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>

                            </ul>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Listas
                </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="altalista1.php">Alta de Lista</a></li>
                                <li><a class="dropdown-item" href="consultalista.php">Consultas/Bajas/Modificaciones</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>

                            </ul>
                        </li>
                        <li class="nav-item">
                       <a class="nav-link disbled" aria-current="page" href="#">Resultados</a>
            
                    </li>
                    <li class="nav-item">
                      <a class="nav-link disbled" aria-current="page" href="#">Habilitacion de mesa</a>
                        
                    </li>
                        
                    </ul>
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </nav>


        </div>






        </div>



        <form method="POST" action="altalista.php" enctype="multipart/form-data">
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre lista</label>
                <input type="text" name="nombredelista" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese el nombre de la lista </div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre presidente</label>
                <input type="text" name="presidente" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre del presidente </div>
            </div>

            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre vicepresidente</label>
                <input type="text" name="vicepresidente" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre del vicepresidente</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre secretario</label>
                <input type="text" name="secretario" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre del secretario </div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre tesorero</label>
                <input type="text" name="tesorero" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre del tesorero </div>

            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre primer vocal</label>
                <input type="text" name="primervocal" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre del primer vocal</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre segundo vocal</label>
                <input type="text" name="segundovocal" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre del segundo vocal </div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre tercer vocal</label>
                <input type="text" name="tercervocal" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre del tercer vocal </div>
            </div>
            <div class="mb-3 container">
                <label>Imagen referencial de la lista
                <input id="archivo" name="archivo" type="file" accept="image/jpeg" />
            </label>
            </div>
            <br>
            <div class="mb-3 form-check container">
                <button type="submit" class="btn btn-primary">ENVIAR</button>
            </div>
        </form><br>




        </form>


        <footer class="bg-light text-center text-lg-start">
            <!-- Copyright -->
            <div class="text-center p-3 container-fluid style=" background-color: rgba(0, 0, 0, 0.2); ">
          © 2021 Copyright: realizado por 7mo - informatica E.E.S.T. N°1 "Don Luis Bussalleu " - Rojas (B)
          
        </div>
        <!-- Copyright -->
      </footer>

</body>
<script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
</html>