-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-10-2021 a las 22:47:52
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `elecciones_2021`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `id_alumno` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `DNI` int(11) NOT NULL,
  `huella` blob NOT NULL COMMENT 'Lector',
  `voto` varchar(1) COLLATE utf8_bin NOT NULL COMMENT 'S(si) N(no) ',
  `huellla2` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Temp de test'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id_alumno`, `nombre`, `DNI`, `huella`, `voto`, `huellla2`) VALUES
(1, 'Martina Estevez', 44667192, '', 'S', '111'),
(2, 'Gianela Cornejo', 45176213, '', 'N', '222'),
(3, 'Paula Navarro', 43866165, '', 'N', '333'),
(4, 'Leandro Diaz ', 44758612, '', 'N', '444');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listas`
--

CREATE TABLE `listas` (
  `id_lista` int(11) NOT NULL,
  `lista` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Denominacion lista',
  `presidente` varchar(100) COLLATE utf8_bin NOT NULL,
  `vicepresidente` varchar(100) COLLATE utf8_bin NOT NULL,
  `secretario` varchar(100) COLLATE utf8_bin NOT NULL,
  `tesorero` varchar(100) COLLATE utf8_bin NOT NULL,
  `primer_vocal` varchar(100) COLLATE utf8_bin NOT NULL,
  `segundo_vocal` varchar(100) COLLATE utf8_bin NOT NULL,
  `tercer_vocal` varchar(100) COLLATE utf8_bin NOT NULL,
  `votos` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'Totaliza votos lista',
  `foto` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'nombre archivo jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `listas`
--

INSERT INTO `listas` (`id_lista`, `lista`, `presidente`, `vicepresidente`, `secretario`, `tesorero`, `primer_vocal`, `segundo_vocal`, `tercer_vocal`, `votos`, `foto`) VALUES
(1, 'uet', 'Leandro Diaz', 'Martina ', 'Gianela', 'Paula ', 'Lourdes', 'Ivan', 'Roman ', '1', 'boleta1.png'),
(2, 'CEP', 'rocio', 'claudia', 'martin', 'leandro', 'uriel', 'jose', 'titania', '0', 'boleta2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turnos`
--

CREATE TABLE `turnos` (
  `id_turno` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `burbuja` int(11) NOT NULL,
  `mesa_votacion` int(11) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `turnos`
--

INSERT INTO `turnos` (`id_turno`, `fecha`, `hora`, `burbuja`, `mesa_votacion`, `descripcion`) VALUES
(1, '2021-10-14', '15:30:00', 28, 1, ''),
(2, '2021-10-14', '16:00:00', 12, 1, ''),
(3, '2021-10-14', '16:30:00', 17, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(50) COLLATE utf8_bin NOT NULL,
  `contraseña` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'cifrada md5\r\ntecnica',
  `nivel` int(11) NOT NULL COMMENT '0 usuario\r\n1 autoridad\r\n2 director'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `contraseña`, `nivel`) VALUES
(1, 'martina', '9805c6fbb42cb82adb63bc6afd8f2039', 1),
(2, 'gianela', '9805c6fbb42cb82adb63bc6afd8f2039', 2),
(3, 'leandro', '9805c6fbb42cb82adb63bc6afd8f2039', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id_alumno`);

--
-- Indices de la tabla `listas`
--
ALTER TABLE `listas`
  ADD PRIMARY KEY (`id_lista`);

--
-- Indices de la tabla `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`id_turno`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `id_alumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `listas`
--
ALTER TABLE `listas`
  MODIFY `id_lista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `turnos`
--
ALTER TABLE `turnos`
  MODIFY `id_turno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
