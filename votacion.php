

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <div class="container-fluid disabled">

     <div class="row">

            <img src = "img/EEST N°1.png" style= "width: 1550px; height: 150px;" >
            <hr>
            <br>
                
            <?php

                $n = $_GET ['nombre'];
                $ida = $_GET['idalumno'];
            ?>


                <div class="alert alert-dark" role="alert">

                     <h2> Alumno: <?php echo $n; ?>  </h2>

                </div>


                    <?php
                        require 'conexion.php';
                        $query= "SELECT * from listas";
                        $result = mysqli_query($con,$query);
                        $c = 0;
                        while($valores= mysqli_fetch_array($result)){

                                echo '<div class="col bg-red">';
                                            $imagen = '<img src = "fotos/'.$valores['foto'].'" style= "width: 600px; height: 250px;">';
                                                echo $imagen;

                                                echo '<div class="mt-4 mr-4 w-80">';
                                                echo '<select name= "integrantes" class="w-100">';
                                                echo '<option value= 0>PRESIDENTE:'.$valores['presidente'].' </option>';
                                                echo '<option value= 1>VICEPRESIDENTE:'.$valores['vicepresidente'].' </option>';
                                                echo '<option value= 2>SECRETARIO:'.$valores['secretario'].' </option>';
                                                echo '<option value= 3>TESORERO:'.$valores['tesorero'].' </option>';
                                                echo '<option value= 4>VOCAL1:'.$valores['vocal1'].' </option>';
                                                echo '<option value= 5>VOCAL2:'.$valores['vocal2'].' </option>';
                                                echo '<option value= 6>VOCAL3:'.$valores['vocal3'].' </option>';
                                                echo '</select>';
                                                echo '</div>';
                                    
                                                echo '<div class="mt-4 ml-0 w-80">';
                                                echo '<input class="btn btn-primary w-75" type="bottom" onclick = "preguntar(' . $valores ['id_lista'] .', '.$ida . ')" style="border-style: 55px; block-size: 40px ; font-size: 25px; text-align: center; margin-left: 15px; margin-bottom: 70px" value="Votar" >';
                                                echo '</div>';
                                echo '</div>';

                        }

                    ?>

                            
        
        </div>

    </div>




<footer class="bg-light text-center text-lg-start">
        <!-- Copyright -->
        <div class="text-center p-3 container-fluid style="background-color: rgba(0, 0, 0, 0.2);>
          © 2021 Copyright: realizado por 7mo - informatica E.E.S.T. N°1 "Don Luis Bussalleu"- Rojas (B)
          
        </div>
        <!-- Copyright -->
      </footer>
</div>

<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>


<script type="text/javascript">

function preguntar(id, ida) {
     
    if(confirm('¿Estás seguro que desas votar lista '+id+'? '))
    {
        window.location.href = "votar.php?lista="+id+"&idalumno="+ida;
    }
}

</script>

</body>

</html>